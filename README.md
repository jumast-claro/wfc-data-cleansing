
Proyecto iniciado en mayo de 2019 para procesar el archivo .csv que baja de SAS (Workflow Client).
Empezó como un par de libros en Jupyter Lab para preparar los datos para armar los reportes en Power BI, y luego los pasé a arhivos .py para facilitar la ejecución.
Tal y como está ahora se pueden generar prefectamente los reportes en Power BI.

Dado que ahora vamos a tener que armar los reportes en Tableau, y que (al menos hasta dónde yo sé) la versión de Tableu que tenemos no tiene las mismas capacidades que Power BI para masajear los datos (no tenemos Tableau Prep), me veo obligado a retomar el proyecto.