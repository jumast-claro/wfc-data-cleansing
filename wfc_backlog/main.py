import pandas as pd
import os
import sys

CURRENT = os.path.dirname(os.path.abspath(__file__))
PARENT = os.path.dirname(CURRENT)
sys.path.append(PARENT)
from src.utils import codigo_provincia, codigo_municipio, etapa, tipo, l2l

FECHA_PROCESO = "FechaProceso"
BANDEJA = "Bandeja"
CALLE = "Calle"
DESCRIPCION = "Descripcion"
DOMICILIO = "Domicilio"
TOMADO_POR = "TomadoPor"
FECHA_CIERRE = "FechaCierre"
ENLACE = "NroEnlace"
ORDEN_CRM = "NroOrdenCRM"
PM_ASIGNADO = "PmAsignado"
CLIENTE = "Cliente"
FECHA_INICIO = "FechaInicio"
ORDEN_SAP = "NroOrdenSAP"
TIPO_INSTALACION = "TipoInstalacion"
CODIGO_PROVINCIA = "CodigoProvincia"
CODIGO_MUNICIPIO = "CodigoMunicipio"
NOMBRE_MUNICIPIO = "NombreMunicipio"
ZONA = "Zona"
PROVINCIA = "Provincia"
CIUDAD = "Ciudad"
TICKET_ID = "TicketID"
TICKET_ID2 = "TicketID2"
PLAZO_INSATALACION = "PlazoInstalacion"


def direccion(domicilio: str) -> str:

    return domicilio.split("/")[0]


def clean_descripcion(descripcion):
    return descripcion.split('-', 1)[1]


def zona(row):

    provincia = row["Provincia"]
    zona = row["Zona"]

    if not pd.isna(zona):
        return zona

    if provincia == "Capital Federal":
        return "AMBA"

    if provincia not in ["Capital Federal", "Buenos Aires"]:
        return "INTE"

    return "?"


def main():

    reposte_sas = pd.read_csv("sas.csv")

    columnas = {
        "PROVINCIA": PROVINCIA,
        "CIUDAD": CIUDAD,
        "TIPO_DE_INSTALACION_D": TIPO_INSTALACION,
        "ANIO_MES": FECHA_PROCESO,
        "ORDEN_CRM": ORDEN_CRM,
        "NRO_SERVICIO": ENLACE,
        "PARQUE_ID_SAP": ORDEN_SAP,
        "BANDEJA": BANDEJA,
        "RAZON_SOCIAL": CLIENTE,
        "DESCRIPCION": DESCRIPCION,
        "PM_ASIGNADO": PM_ASIGNADO,
        "DUENO": TOMADO_POR,
        "DOMICILIO": DOMICILIO,
        "FECHA_RECEPCION_INBOX": FECHA_INICIO,
        "FECHA_FIN_INBOX": FECHA_CIERRE,
        "TICKET_ID": TICKET_ID,
        "TICKET_ID_2": TICKET_ID2,
        "PLAZO_DE_INSTALACION": PLAZO_INSATALACION,
    }

    reposte_sas = reposte_sas[columnas.keys()]
    reposte_sas = reposte_sas.rename(columns=columnas, inplace=False)

    predicate1 = reposte_sas[BANDEJA] == "InstalacionesFO"
  
    predicate3 = reposte_sas[FECHA_CIERRE] == "."
    predicate = predicate1 & predicate3
    reposte_sas = reposte_sas[predicate]

    columnas = [
        TIPO_INSTALACION,
        ORDEN_CRM,
        ORDEN_SAP,
        ENLACE,
        CLIENTE,
        DESCRIPCION,
        PM_ASIGNADO,
        TOMADO_POR,
        PROVINCIA,
        CIUDAD,
        DOMICILIO,
        FECHA_INICIO,
        CODIGO_PROVINCIA,
        CODIGO_MUNICIPIO,
        TICKET_ID,
        PLAZO_INSATALACION,
        NOMBRE_MUNICIPIO,
        "Zona",
        "Etapa",
        "Tipo",
        "Direccion",
        TICKET_ID2,
        "LanToLan"

    ]

    reposte_sas[CODIGO_PROVINCIA] = reposte_sas[DOMICILIO].apply(codigo_provincia)
    reposte_sas[CODIGO_MUNICIPIO] = reposte_sas[DOMICILIO].apply(codigo_municipio)

    reposte_sas[ORDEN_CRM] = reposte_sas[ORDEN_CRM].astype("int64")
    reposte_sas[ORDEN_SAP] = reposte_sas[ORDEN_SAP].astype("int64")
    # reposte_sas[ENLACE] = reposte_sas[ENLACE].astype("int64")
    reposte_sas[PLAZO_INSATALACION] = reposte_sas[PLAZO_INSATALACION].astype("int64")
    reposte_sas[FECHA_INICIO] = reposte_sas[FECHA_INICIO].astype("datetime64")


    reposte_sas[CODIGO_MUNICIPIO] = reposte_sas[DOMICILIO].apply(lambda x: codigo_municipio(x))

    municipios = pd.read_csv("municipios.csv", dtype={"id": str})[["id", "nombre"]]
    municipios.columns = [CODIGO_MUNICIPIO, NOMBRE_MUNICIPIO]
    reposte_sas = reposte_sas.merge(municipios, on=CODIGO_MUNICIPIO, how='left')

    reposte_sas.sort_values(by=[FECHA_INICIO], inplace=True)    

    zona_por_municipio = pd.read_excel("zona_por_municipio.xlsx", dtype={"CodigoMunicipio": str})[["CodigoMunicipio", "Zona"]]
    reposte_sas = reposte_sas.merge(zona_por_municipio, on=CODIGO_MUNICIPIO, how='left')

    reposte_sas["Zona"] = reposte_sas.apply(lambda row: zona(row), axis=1)

    reposte_sas[NOMBRE_MUNICIPIO] = reposte_sas[NOMBRE_MUNICIPIO].fillna("")
    reposte_sas[TOMADO_POR] = reposte_sas[TOMADO_POR].fillna("")

    reposte_sas["Etapa"] = reposte_sas[DESCRIPCION].apply(lambda x: etapa(x))
    reposte_sas["Tipo"] = reposte_sas[DESCRIPCION].apply(lambda x: tipo(x))
    reposte_sas[DESCRIPCION] = reposte_sas[DESCRIPCION].apply(lambda x: clean_descripcion(x))
    reposte_sas["Direccion"] = reposte_sas[DOMICILIO].apply(lambda x: direccion(x))
    reposte_sas["LanToLan"] = reposte_sas[DESCRIPCION].apply(lambda x: l2l(x))

    reposte_sas = reposte_sas[columnas]
    # reposte_sas.to_excel("wfc-backlog.xlsx", index=False, float_format='%.0f')
    reposte_sas.to_json("wfc-backlog.json", orient="table")


if __name__ == "__main__":
    main()