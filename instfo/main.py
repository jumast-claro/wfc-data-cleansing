import csv
import pandas as pd
import datetime


def clean_date(s):
    if type(s) is not datetime.datetime:
        return None

    return s


def calcular_tipo_proyecto(partido, tecnologia):
    if partido == "Tigre" and tecnologia == "G-PON - Multi":
        return "TIGRE"
    return "CLIENTE"


def clean_cantidad(cantidad):

    if pd.isna(cantidad):
        return 0
    return cantidad


def caseins_in(s, l):
    return s.lower() in (item.lower() for item in l)


def calculcar_tipificacion(tarea):

    if caseins_in(tarea, ["OC", "TA", "TA + OC"]):
        return "NoAcometido"
    elif caseins_in(tarea, ["ODF", "Sin Tareas", "Tendido", "Tendido + Obstrucciones"]):
        return "Acometido"
    else:
        return None


def clean_prioridad(priorizada):
    if priorizada in ["No", "Np", "no", "nO", "TF", "NO"] or pd.isna(priorizada):
        return "No"
    elif priorizada in ["Si", "Sí", "si", "sI", "sí", "SI"]:
        return "Sí"
    elif priorizada in ["SR", "", " ", None]:
        return "SinRegistro"
    else:
        return REVISAR_FORNULA


def calcular_zona(partido):
    return "CABA" if partido == "Cap. Fed." else "AMBA"


def calcular_con_tareas(tarea):

    if tarea == "Sin Tareas":
        return "No"
    return "Sí"


REVISAR_FORNULA = "RevisarFórmnula"

FECHA_INICIO = "FechaInicio"
FECHA_CIERRE = "FechaCierre"
TIPO_PROYECTO = "TipoProyecto"
METROS_TENDIDO = "MetrosTendido"
METROS_OBRA = "MetrosObraCivil"
TIPIFICACION = "Tipificacion"
TIPO_TAREA = "TipoTarea"
PRIORIZADA = "Priorizada"
ZONA = "Zona"
PARTIDO = "Partido"
CIERRE_MES = "MesCierre"
CIERRE_ANIO = "AñoCierre"
CON_TAREAS = "ConTareas?"



def main():

    cerradas_instfo = pd.read_excel("Seguimiento Instalaciones FO 05-03-2020.xlsx", sheet_name="Terminadas")

    columnas = {
        "CLIENTE": "Cliente",
        "DIRECCION": "Direccion",
        "Partido": PARTIDO,
        "Tarea Recibida": FECHA_INICIO,
        "Tipo orden": "TipoOrden",
        "Tecnología": "Tecnologia",
        "Metros obra civil": METROS_OBRA,
        "Metros tendido": METROS_TENDIDO,
        "Prioridad?": PRIORIZADA,
        "Fecha de Cierre de tarea": FECHA_CIERRE,
        "Tarea": TIPO_TAREA
    }

    cerradas_instfo = cerradas_instfo[columnas.keys()]
    cerradas_instfo = cerradas_instfo.rename(columns=columnas, inplace=False)

    cerradas_instfo[FECHA_INICIO] = cerradas_instfo[FECHA_INICIO].apply(lambda x: clean_date(x))
    cerradas_instfo[FECHA_CIERRE] = cerradas_instfo[FECHA_CIERRE].apply(lambda x: clean_date(x))

    predicate = cerradas_instfo[FECHA_CIERRE].dt.year == 2020
    cerradas_instfo = cerradas_instfo[predicate]
    cerradas_instfo[TIPO_PROYECTO] = cerradas_instfo.apply(lambda x: calcular_tipo_proyecto(x.Partido, x.Tecnologia), axis=1)

    cerradas_instfo[METROS_OBRA] = cerradas_instfo[METROS_OBRA].apply(lambda x: clean_cantidad(x))
    cerradas_instfo[METROS_TENDIDO] = cerradas_instfo[METROS_TENDIDO].apply(lambda x: clean_cantidad(x))
    cerradas_instfo[TIPIFICACION] = cerradas_instfo.apply(lambda x: calculcar_tipificacion(x[TIPO_TAREA]), axis=1)
    cerradas_instfo[PRIORIZADA] = cerradas_instfo[PRIORIZADA].apply(lambda x: clean_prioridad(x))
    cerradas_instfo[ZONA] = cerradas_instfo[PARTIDO].apply(lambda x: calcular_zona(x))

    cerradas_instfo[CIERRE_ANIO] = cerradas_instfo[FECHA_CIERRE].dt.year
    cerradas_instfo[CIERRE_MES] = cerradas_instfo[FECHA_CIERRE].dt.month
    cerradas_instfo[CON_TAREAS] = cerradas_instfo[TIPO_TAREA].apply(lambda x: calcular_con_tareas(x))

    cerradas_instfo.to_excel("out_instfo-cerradas.xlsx", index=False, float_format='%.0f')



if __name__ == "__main__":
    main()