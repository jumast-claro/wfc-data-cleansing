import datetime


def to_date_time(s: str) -> datetime:

    if s == '.':
        return None

    # meses = ['dummy', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']

    meses = ['dummy', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    x = s.split(' ')

    if len(x) == 2:
        return datetime.datetime.strptime(s, '%d/%m/%Y %H:%M:%S')

    day = x[1].replace(',', '')
    month = meses.index(x[0])
    year = x[2]

    HH_MM_SS = x[3]
    AM_PM = x[4]

    d = f'{month} {day}, {year} {HH_MM_SS} {AM_PM}'
    format = '%m %d, %Y %I:%M:%S %p'

    return datetime.datetime.strptime(d, format)


def zona(codigo_prov: str) -> str:

    if codigo_prov in ["AR-B", "AR-C"]:
        return "AMBA"

    return "INTE"


def codigo_provincia(domicilio: str) -> str:

    if "Salta AR" in domicilio:
        return "AR-A"

    if "Buenos Aires AR" in domicilio:
        return "AR-B"

    if "Capital Federal AR" in domicilio:
        return "AR-C"

    if "San Luis AR" in domicilio:
        return "AR-D"

    if "La Rioja AR" in domicilio:
        return "AR-F"

    if "Santiago del Estero AR" in domicilio:
        return "AR-G"

    if "Catamarca AR" in domicilio:
        return "AR-K"

    if "Mendoza AR" in domicilio:
        return "AR-M"

    if "Neuquen AR" in domicilio:
        return "AR-Q"

    if "Santa Fe AR" in domicilio:
        return "AR-S"

    if "Tucuman AR" in domicilio:
        return "AR-T"

    if "Chubut AR" in domicilio:
        return "AR-U"

    if "Cordoba AR" in domicilio:
        return "AR-X"

    if "Santa Cruz AR" in domicilio:
        return "AR-Z"


def codigo_municipio(domicilio: str) -> str:

    if "9 DE JULIO Buenos Aires AR" in domicilio:
        return '060588'

    if "ALMIRANTE BROWN Buenos Aires AR" in domicilio:
        return '060028'

    if "BERAZATEGUI Buenos Aires AR" in domicilio:
        return "060091"

    if "campana Buenos Aires AR" in domicilio:
        return '060126'

    if "ESCOBAR Buenos Aires AR" in domicilio or "Garin Buenos Aires AR" in domicilio or "Maschwitz Buenos Aires AR" in domicilio:
        return '060252'

    if "ESTEBAN ECHEVERRIA Buenos Aires AR" in domicilio or "9 DE ABRIL Buenos Aires AR" in domicilio or "Esteban Echeverria Buenos Aires AR" in domicilio:
        return '060260'

    if "GENERAL SAN MARTIN Buenos Aires AR" in domicilio or "San Andres Buenos Aires AR" in domicilio or "Villa Ballester Buenos Aires AR" in domicilio:
        return "060371"

    if "Valentin Alsina Buenos Aires AR" in domicilio or "LANUS Buenos Aires AR" in domicilio:
        return '060434'

    if "alvinas argentinas Buenos Aires AR" in domicilio or "Ing. Pablo Nogues Buenos Aires AR" in domicilio or "MALVINAS ARGENTINAS Buenos Aires AR" in domicilio or "Malvinas Argentinas Buenos Aires AR" in domicilio or "Tortuguitas Buenos Aires AR" in domicilio:
        return '060515'

    if "MORENO Buenos Aires AR" in domicilio or "La Reja Buenos Aires AR" in domicilio or "LA REJA Buenos Aires AR" in domicilio:
        return '060560'

    if "MORON Buenos Aires AR" in domicilio:
        return '060568'

    if "Pilar Buenos Aires AR" in domicilio or "PILAR Buenos Aires AR" in domicilio or "PIlar Buenos Aires AR" in domicilio or "Fatima Buenos Aires AR" in domicilio or "FATIMA Buenos Aires AR" in domicilio:
        return '060638'

    if "SAN ISIDRO Buenos Aires AR" in domicilio or "Beccar Buenos Aires AR" in domicilio or "Martinez Buenos Aires AR" in domicilio:
        return '060756'

    if "Tigre Buenos Aires AR" in domicilio or \
            "TIGRE Buenos Aires AR" in domicilio or \
            "GENERAL PACHECO Buenos Aires AR" in domicilio or \
            "PACHECO Buenos Aires AR" in domicilio or \
            "Pacheco Buenos Aires AR" in domicilio or \
            "benavidez Buenos Aires AR" in domicilio or \
            "El Talar Buenos Aires AR" in domicilio or \
            "tigre Buenos Aires AR" in domicilio or \
            "Pacheco, Buenos Aires AR" in domicilio or \
            "NORDELTA Buenos Aires AR" in domicilio or \
            "DON TORCUATO Buenos Aires AR" in domicilio or \
            "RICARDO ROJAS Buenos Aires AR" in domicilio or \
            "Benavidez Buenos Aires AR" in domicilio:
        return '060805'

    if "TRES DE FEBRERO Buenos Aires AR" in domicilio or "Villa Bosch" in domicilio or "SANTOS LUGARES Buenos Aires AR" in domicilio:
        return '060840'

    if "VICENTE LOPEZ Buenos Aires AR" in domicilio or "Olivos Buenos Aires AR" in domicilio:
        return '060861'

    if "Chivilcoy Buenos Aires AR" in domicilio:
        return "060224"

    if "TRENQUE LAUQUEN Buenos Aires AR" in domicilio:
        return "060826"

    if "Bella Vista Buenos Aires AR" in domicilio:
        return "060760"

    if "TANDIL Buenos Aires AR" in domicilio:
        return '060791'

    if "Tortugitas Buenos Aires AR" in domicilio:
        return '060515'

    if "Melchor Romero Buenos Aires AR" in domicilio:
        return '060441'

    if "El Jaguel Buenos Aires AR" in domicilio:
        return "060260"

    if "MAR DEL PLATA Buenos Aires AR" in domicilio or "GENERAL PUEYRREDON Buenos Aires AR" in domicilio or "Mar del Plata Buenos Aires AR" in domicilio or "mar del plata" in domicilio:
        return "060357"

    if "El Pato Buenos Aires AR" in domicilio:
        return "060091"

    if "MAR CHIQUITA Buenos Aires AR" in domicilio:
        return "060518"

    if "LA PLATA Buenos Aires AR" in domicilio:
        return "060441"

    if "SAN FERNANDO Buenos Aires AR" in domicilio:
        return "060749"

    if "ITUZAINGO Buenos Aires AR" in domicilio:
        return "060410"

    if "FLORENCIO VARELA Buenos Aires AR" in domicilio:
        return "060274"

    if "CAMPANA Buenos Aires AR" in domicilio:
        return "060126"

    if "zarate Buenos Aires AR" in domicilio or "Zarate Buenos Aires AR" in domicilio or "ZARATE Buenos Aires AR" in domicilio:
        return "060882"

    if "LA MATANZA Buenos Aires AR" in domicilio or "gregorio de laferrere Buenos Aires AR" in domicilio:
        return "060427"

    if "QUILMES Buenos Aires AR" in domicilio:
        return "060658" 

    if "AVELLANEDA Buenos Aires AR" in domicilio:
        return "060035"

    if "Villarino Viejo Buenos Aires AR" in domicilio:
        return "060875"

    if "VILLA BOSCH Buenos Aires AR" in domicilio:
        return "060840"

    if "OLAVARRIA Buenos Aires AR" in domicilio:
        return "060595"

    if "LUJAN Buenos Aires AR" in domicilio:
        return "060497"

    if "SAN PEDRO Buenos Aires AR" in domicilio:
        return "060770"

    if "MERLO Buenos Aires AR" in domicilio:
        return "060539"

    if "Moron Buenos Aires AR" in domicilio:
        return "060568"

    if "CORONEL ROSALES Buenos Aires AR" in domicilio:
        return "060182"

    if "BAHIA BLANCA Buenos Aires AR" in domicilio:
        return "060056"

    if "JOSE CLEMENTE PAZ Buenos Aires AR" in domicilio:
        return "060412"

    if "Pehuajo Buenos Aires AR" in domicilio:
        return "060609"

    if "san miguel Buenos Aires AR" in domicilio:
        return "060760"

    if "EZEIZA Buenos Aires AR" in domicilio:
        return "060270"

    if "PERGAMINO Buenos Aires AR" in domicilio:
        return "060623"

    if "COLON Buenos Aires AR" in domicilio or "Colon Buenos Aires AR" in domicilio:
        return "060175"

    if "LA COSTA Buenos Aires AR" in domicilio:
        return "060420"


def l2l(descripcion: str) -> str:

    if descripcion == "":
        return ""

    # if pd.isna(descripcion):
    #     return ""

    if "SITIO A" in descripcion:
        return "A"

    if "SITIO B" in descripcion:
        return "B"
    else:
        return ""


def etapa(descripcion: str) -> str:

    DEFINICION = "Definicion de proyecto"
    TENDIDOS = "Tendidos"
    OBRA_CIVIL = "Realizar obra civil"

    if DEFINICION in descripcion:
        return "Definición de proyecto"

    if "Realizar obra civil" in descripcion:
        return "Obra civil"

    if "Permisos municipales" in descripcion:
        return "Permisos municipales"

    if TENDIDOS in descripcion:
        return TENDIDOS

    if "Cancelacion" in descripcion:
        return "Cancelación"

    if "(anexar-copia)" in descripcion:
        return "Anexada"

    return "?"


def tipo(descripcion: str) -> str:

    splitted = descripcion.split("-")
    if len(splitted) >= 3:
        return splitted[1].strip()
    return "?"






