import sys
import os
CURRENT = os.path.dirname(os.path.abspath(__file__))
PARENT = os.path.dirname(CURRENT)
sys.path.append(PARENT)

import csv
import pandas as pd
from src.A_Clean import clean
from src.B_InstalacionesFO import procesar


def main():

    archivo_sas = sys.argv[1]
    archivo_tomado_por = sys.argv[2]

    tareas = pd.read_csv(archivo_sas)
    adicional_tomadopor = pd.read_excel(archivo_tomado_por)

    tareas = clean(tareas)
    resultados = procesar(tareas, adicional_tomadopor)

    cerradas_instalaciones = resultados[0]
    ids = resultados[1]
    ubicacion = resultados[2]

    cerradas_instalaciones.to_csv("out_instfo-cerradas.csv", index=False, sep=',', quoting=csv.QUOTE_ALL, quotechar='"', float_format='%.0f')
    ids.to_csv("out_instfo-cerradas-permanencia.csv", index=False, sep=',', quoting=csv.QUOTE_ALL, quotechar='"', float_format='%.0f')
    ubicacion.to_csv("out_instfo-cerradas-ubicacion.csv", index=False, sep=',', quoting=csv.QUOTE_ALL, quotechar='"', float_format='%.0f')


if __name__ == "__main__":
    main()
