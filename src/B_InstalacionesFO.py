import pandas as pd
import datetime
from src.utils import codigo_provincia, codigo_municipio, zona


def custom(x):

    ans = ''
    for n in x:
        ans = ans + '-' + str(n)
    return ans.lstrip('-')


def obtener_cerradas(tareas: pd.DataFrame) -> pd.DataFrame:

    predicate1 = tareas["Bandeja"] == "InstalacionesFO"
    predicate2 = tareas["Descripcion"].str.contains("Tendidos")
    predicate = predicate1 & predicate2
    cerradas_instalaciones = tareas[predicate]

    dic = {
        'Bandeja': 'first',
        'NroOrden': 'first',
        'NroEnlace': 'first',
        'NroIdSap': 'first',
        'FechaCierre': 'max',
        'Estado': 'last',
        'Cliente': 'last',
        'PlazoInstalacion': 'last',
        'PM': 'last',
        'TomadoPor': 'first',
        'MotivoAlta': 'last',
        'UnidadDeNegocio': 'last',
        'Ejecutivo': 'last',
        'Familia': 'last',
        'Segmento': 'last',
        'Pais': 'last',
        'Region': 'last',
        'Provincia': 'last',
        'Ciudad': 'last',
        'Calle': 'last',
        'Domicilio': 'last',
        'Nro': 'last',
        'Piso': 'last',
    }

    SENTINELA = datetime.datetime(2100, 1, 1)

    cerradas_instalaciones = cerradas_instalaciones.sort_values(by='FechaCierre')
    cerradas_instalaciones = cerradas_instalaciones.drop_duplicates(subset=['TareaID'], keep='first')
    cerradas_instalaciones['FechaCierre'].fillna(SENTINELA, inplace=True)
    cerradas_instalaciones = cerradas_instalaciones.sort_values(by='FechaCierre')

    aux_list = list(dic.keys())
    aux_list.insert(1, 'TicketID')
    cerradas_instalaciones = cerradas_instalaciones.groupby(['TicketID'], as_index=False).agg(dic)[aux_list]

    predicate3 = cerradas_instalaciones["FechaCierre"] != SENTINELA
    cerradas_instalaciones = cerradas_instalaciones[predicate3]

    return cerradas_instalaciones


def agregar_supervisor(tareas: pd.DataFrame) -> pd.DataFrame:

    daniel_cereser = [
        "Eduardo Ongania",
        "Mariano Filipowicz",
        "Juan Manuel Straus",
        "Fabian Cardozo",
        "Estefania Kramarenko",
        "Nicolas Ariel Dominguez",
        "Sebastian Urdanavia"
    ]

    tareas["Supervisor"] = tareas["TomadoPor"]
    tareas["Supervisor"] = tareas["Supervisor"].apply(lambda x: "Daniel Cereser" if x in daniel_cereser else "desconocido")

    return tareas


def obtener_ids(tareas: pd.DataFrame) -> pd.DataFrame:

    ids = pd.DataFrame(tareas['TicketID'])
    ids.drop_duplicates('TicketID', inplace=True)
    return ids


def agregar_info_permisos(ids: pd.DataFrame, tareas: pd.DataFrame) -> pd.DataFrame:

    predicate = tareas["Descripcion"].str.contains("Permisos municipales", na=False)
    permisos = pd.DataFrame(tareas[predicate]['TicketID'])
    permisos = permisos.drop_duplicates(subset=['TicketID'])
    permisos["Permisos?"] = True

    ids = ids.merge(permisos, on=['TicketID'], how='left', validate='one_to_one')
    ids.fillna(value={'Permisos?': False}, inplace=True)

    return ids


def agregar_info_inserciones(ids: pd.DataFrame, tareas: pd.DataFrame) -> pd.DataFrame:

    p = tareas["Descripcion"].str.contains("Realizar empalmes", na=False)
    inserciones = pd.DataFrame(tareas[p]['TicketID'])
    inserciones = inserciones.drop_duplicates(subset=['TicketID'])
    inserciones["Inserciones?"] = True

    ids = ids.merge(inserciones, on=['TicketID'], how='left', validate='one_to_one')
    ids.fillna(value={'Inserciones?': False}, inplace=True)

    return ids


def agregar_info_permamencia(ids: pd.DataFrame, tareas: pd.DataFrame) -> pd.DataFrame:

    predicate1 = tareas["Bandeja"] == "InstalacionesFO"
    predicate2 = ~pd.isnull(tareas["FechaCierre"])
    predicate3 = ~tareas['Descripcion'].str.contains('Cancelacion', na=False)
    predicate = predicate1 & predicate2 & predicate3
    instalaciones = tareas[predicate][['TicketID', 'FechaInicio', 'FechaCierre', 'Estado', 'PermanenciaEnDias']]
    instalaciones = instalaciones.sort_values(by='FechaCierre')

    agrupadas = instalaciones.groupby(['TicketID'], as_index=False).agg({'FechaInicio': 'min', 'FechaCierre': 'max', 'Estado': 'last', 'PermanenciaEnDias': 'sum'})
    agrupadas.rename(columns={'FechaInicio': 'Permanencia.FechaInicio', 'FechaCierre': 'Permanencia.FechaCierre', 'PermanenciaEnDias': 'Permanencia.Suma'}, inplace=True)
    agrupadas['Permanencia.Bruta'] = (agrupadas['Permanencia.FechaCierre'] - agrupadas['Permanencia.FechaInicio']).dt.days
    ids = ids.merge(agrupadas, on=['TicketID'], how='left', validate='one_to_one')

    predicate1 = tareas["Descripcion"].str.contains("Permisos municipales", na=False)
    predicate2 = ~pd.isnull(tareas["FechaCierre"])
    predicate3 = ~tareas['Descripcion'].str.contains('Cancelacion', na=False)
    predicate = predicate1 & predicate2 & predicate3
    permisos = tareas[predicate][['TicketID', 'PermanenciaEnDias']]
    permisos = permisos.drop_duplicates(subset=['TicketID'])
    permisos = permisos.groupby(['TicketID'], as_index=False).agg({'PermanenciaEnDias': 'sum'})
    permisos = permisos.rename(columns={'PermanenciaEnDias': 'Permanencia.EnPermisos'})
    ids = ids.merge(permisos, on=['TicketID'], how='left', validate='one_to_one')
    ids = ids.fillna(value={'PermanenciaEnPermisos': 0})

    ids['Permanencia.Neta'] = ids['Permanencia.Bruta'] - ids['Permanencia.EnPermisos']

    ids['Permanencia.AnioCierre'] = ids['Permanencia.FechaCierre'].dt.year
    ids['Permanencia.MesCierre'] = ids['Permanencia.FechaCierre'].dt.month

    return ids


def procesar(tareas: pd.DataFrame, adicional_tomadopor: pd.DataFrame) -> []:

    cerradas_instalaciones = obtener_cerradas(tareas)

    adicional_tomadopor['TicketID'] = adicional_tomadopor['TicketID'].astype('str')
    tomadoPor = pd.DataFrame(cerradas_instalaciones[['TicketID', 'TomadoPor']])
    tomadoPor = tomadoPor.merge(adicional_tomadopor, on='TicketID', how='left', validate='one_to_many')
    tomadoPor['TomadoPorDef'] = tomadoPor['TomadoPor'].combine(tomadoPor['TomadoPor-BIS'], lambda x, y: y if not pd.isna(y) else x)
    tomadoPor = tomadoPor[['TicketID', 'TomadoPorDef']]
    cerradas_instalaciones = cerradas_instalaciones.merge(tomadoPor, on='TicketID', how='left', validate='one_to_many')
    cerradas_instalaciones = cerradas_instalaciones.rename(columns={'TomadoPor': 'TomadoPor_Original', 'TomadoPorDef': 'TomadoPor'})

    #cerradas_instalaciones = agregar_supervisor(cerradas_instalaciones)
    cerradas_instalaciones["CodigoProvincia"] = cerradas_instalaciones["Domicilio"].apply(codigo_provincia)
    cerradas_instalaciones["CodigoMunicipio"] = cerradas_instalaciones["Domicilio"].apply(codigo_municipio)
    cerradas_instalaciones["Zona"] = cerradas_instalaciones["CodigoProvincia"].apply(zona)

    ids = obtener_ids(cerradas_instalaciones)
    ids = agregar_info_permisos(ids, tareas)
    ids = agregar_info_inserciones(ids, tareas)
    ids = agregar_info_permamencia(ids, tareas)

    c = ['Pais', 'Region', 'Provincia', 'Ciudad', 'Calle', 'Domicilio', 'Nro', 'Piso']
    ubicacion = cerradas_instalaciones[['TicketID'] + ['CodigoProvincia', 'CodigoMunicipio'] + c]
    cerradas_instalaciones.drop(columns=c, inplace=True)

    return [cerradas_instalaciones, ids, ubicacion]



