import csv

import pandas as pd
from src import utils


def leer_csv() -> pd.DataFrame:
    return pd.read_csv('../tareas_sas.csv')


def excluir_columnas(tareas: pd.DataFrame) -> pd.DataFrame:

    columnas_excluidas = [
        "ANCHO_DE_BANDA",
        "CODIGO_POSTAL",
        "FECHA_ACEPTACION",
        "FECHA_ACEPTACION_GANTT",
        "FECHA_ACTIVACION",
        "FECHA_COMPROMETIDA",
        "FECHA_CONFORMIDAD",
        "FECHA_DE_FACTURACION",
        "FECHA_OPERATIVA",
        "FECHA_VENCIMIENTO",
        "GPON_MAC",
        "MOTIVO",
        "MOTIVO_FRACASO",
        "MOTIVO_INTERRUPCION",
        "MOTIVO_OFFTIME",
        "MOTIVO_RENEGOCIACION",
        "NRO_ENLACE_INTERNACIONAL",
        "NRO_ORDEN_INTERNACIONAL",
        "TECNOLOGIA_CRM",
        "TICKET_ID_2",
        "TIEMPO_EN_BANDEJA",
        "ID_GRUPO",
        "REQUIERE_ASIGNACION_DE_FO",
        "REQUIERE_PLANTA_INTERNA"
    ]
    return tareas.drop(columns=columnas_excluidas, inplace=False)


def excluir_filas(tareas: pd.DataFrame) -> pd.DataFrame:
    predicate1 = ~tareas["DESCRIPCION"].str.contains("MANEJO DE ERROR", na=False)
    predciate2 = ~tareas["RAZON_SOCIAL"].str.contains("DUMMY", na=False)
    predicate3 = ~pd.isnull(tareas["NRO_SERVICIO"])
    predicate = predicate1 & predciate2 & predicate3
    return tareas[predicate]


def convertir_fechas(tareas: pd.DataFrame) -> pd.DataFrame:
    tareas["FECHA_RECEPCION_INBOX"] = tareas["FECHA_RECEPCION_INBOX"].apply(lambda x: utils.to_date_time(x))
    tareas["FECHA_FIN_INBOX"] = tareas["FECHA_FIN_INBOX"].apply(lambda x: utils.to_date_time(x))
    return tareas


def ordenar_columnas(tareas: pd.DataFrame) -> pd.DataFrame:

    dire = [
        "PAIS",
        "REGION",
        "PROVINCIA",
        "CIUDAD",
        "CALLE",
        "DOMICILIO",
        "NRO",
        "PISO"
    ]

    columns = [
        "BANDEJA",
        "ANIO_MES",
        "TareaID",
        "ANIO_CIERRE",
        "MES_CIERRE",
        "TICKET_ID",
        "SUBTAREA",
        "ORDEN_CRM",
        "PARQUE_ID_SAP",
        "NRO_SERVICIO",
        "DESCRIPCION",
        "FECHA_RECEPCION_INBOX",
        "FECHA_FIN_INBOX",
        "PERMANENCIA",
        "PLAZO_DE_INSTALACION",
        "MOTIVO_ALTA",
        "RAZON_SOCIAL",
        "CUIT",
        "PM_ASIGNADO",
        "DUENO",
        "ESTADO"
    ]

    comercial = [
        "UNIDAD_DE_NEGOCIO",
        "EJECUTIVO",
        "FAMILIA",
        "SEGMENTO",
    ]

    return tareas[columns + comercial + dire]


def renombrar_columnas(tareas: pd.DataFrame) -> pd.DataFrame:

    dic = {
        "ANIO_CIERRE": "AnioCierre",
        "MES_CIERRE": "MesCierre",
        "TICKET_ID": "TicketID",
        "SUBTAREA": "Subtarea",
        "ORDEN_CRM": "NroOrden",
        "PARQUE_ID_SAP": "NroIdSap",
        "NRO_SERVICIO": "NroEnlace",
        "BANDEJA": "Bandeja",
        "DESCRIPCION": "Descripcion",
        "FECHA_RECEPCION_INBOX": "FechaInicio",
        "FECHA_FIN_INBOX": "FechaCierre",
        "PERMANENCIA": "PermanenciaEnDias",
        "PLAZO_DE_INSTALACION": "PlazoInstalacion",
        "MOTIVO_ALTA": "MotivoAlta",
        "RAZON_SOCIAL": "Cliente",
        "CUIT": "CUIT",
        "PM_ASIGNADO": "PM",
        "DUENO": "TomadoPor",
        "ESTADO": "Estado",
        "PAIS": "Pais",
        "REGION": "Region",
        "PROVINCIA": "Provincia",
        "CIUDAD": "Ciudad",
        "CALLE": "Calle",
        "DOMICILIO": "Domicilio",
        "NRO": "Nro",
        "PISO": "Piso",
        "UNIDAD_DE_NEGOCIO": "UnidadDeNegocio",
        "EJECUTIVO": "Ejecutivo",
        "FAMILIA": "Familia",
        "SEGMENTO": "Segmento"
    }
    return tareas.rename(columns=dic, inplace=False)


def clean(tareas: pd.DataFrame) -> pd.DataFrame:

    tareas = excluir_columnas(tareas)
    tareas = excluir_filas(tareas)
    tareas = convertir_fechas(tareas)

    tareas["TareaID"] = tareas["TICKET_ID"].astype(str) + "-" + tareas["SUBTAREA"].astype(str)
    tareas['PERMANENCIA'] = (tareas['FECHA_FIN_INBOX'] - tareas['FECHA_RECEPCION_INBOX']).dt.days
    tareas['ANIO_CIERRE'] = (tareas['FECHA_FIN_INBOX']).dt.year
    tareas['MES_CIERRE'] = (tareas['FECHA_FIN_INBOX']).dt.month

    tareas['aux'] = tareas['DESCRIPCION'].apply(utils.l2l)

    tareas['TICKET_ID'] = tareas['TICKET_ID'].astype(str) + '-' + tareas['aux']
    tareas['TICKET_ID'] = tareas['TICKET_ID'].str.rstrip('-')
    tareas.drop(columns=['aux'])

    tareas = ordenar_columnas(tareas)
    tareas = renombrar_columnas(tareas)

    tareas = tareas.drop_duplicates(keep="first")
    tareas = tareas.drop_duplicates(subset=(['TareaID', "FechaInicio", "FechaCierre"]))

    tareas.to_csv("../tareas-sas-clean.csv", index=False, sep=',', quoting=csv.QUOTE_ALL, quotechar='"', float_format='%.0f')
    return tareas
