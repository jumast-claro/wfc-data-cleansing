import unittest
import os
import sys
CURRENT = os.path.dirname(os.path.abspath(__file__))
PARENT = os.path.dirname(CURRENT)
sys.path.append(PARENT)

from src.utils import codigo_municipio


class TestMunicipio(unittest.TestCase):

    def testMunicipio_domicilioContiene_9DEJULIOBuenosAiresAR(self):

        # Arrange
        domicilio = "AV GRAL JULIO DE VEDIA 607 B6500DGG 9 DE JULIO / 9 DE JULIO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060588")

    def testMunicipio_domicilioContiene_ALMIRANTEBROWNBuenosAiresAR(self):

        # Arrange
        domicilio = "MORENO CABO PRIMERO RAMON A 2240 B1852LEH BURZACO / ALMIRANTE BROWN Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060028")

    def testMunicipio_domicilioContiene_BERAZATEGUIBuenosAiresAR(self):

        # Arrange
        domicilio = "AV. ANTARTIDA ARGENTINA Y CALLE 258 0 1886 RANELAGH / BERAZATEGUI Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060091")

    def testMunicipio_domicilioContiene_campanaBuenosAiresAR(self):
        # Arrange
        domicilio = "RUTA 6 KM 193 - C.P.: 2804 0 2804 campana Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060126")

    def testMunicipio_domicilioContiene_ESCOBARBuenosAiresAR(self):
        # Arrange
        domicilio = "LOS INMIGRANTES 1301 B1625AYD BELEN DE ESCOBAR / ESCOBAR Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060252")

    def testMunicipio_domicilioContiene_ESTEBANECHEVERRIABuenosAiresAR(self):
        # Arrange
        domicilio = "LAGOS GARCIA LUIS 4460 B1839GGH 9 DE ABRIL / ESTEBAN ECHEVERRIA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060260")

    def testMunicipio_domicilioContiene_GENERALSANMARTINBuenosAiresAR(self):
        # Arrange
        domicilio = "ROSAS BRIGADIER GENERAL JUAN MANUEL DE 1375 B1655IRB JOSE LEON SUAREZ / GENERAL SAN MARTIN Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060371")

    def testMunicipio_domicilioContiene_ValentinAlsinaBuenosAiresAR(self):
        # Arrange
        domicilio = "Av. Crotti 200 1822 Valentin Alsina Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060434")

    def testMunicipio_domicilioContiene_malvinasargentinasBuenosAiresAR(self):
        # Arrange
        domicilio = "olivos 3764 7500 malvinas argentinas Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060515")

    def testMunicipio_domicilioContiene_MORENOBuenosAiresAR(self):
        # Arrange
        domicilio = "CORRIENTES (entre 2 de Abril y Guillermo Marconi) 5202 B1742HML PASO DEL REY / MORENO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060560")

    def testMunicipio_domicilioContiene_MORONBuenosAiresAR(self):
        # Arrange
        domicilio = "BELGRANO GENERAL MANUEL 236 B1708IFF MORON / MORON Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060568")

    def testMunicipio_domicilioContiene_PilarBuenosAiresAR(self):
        # Arrange
        domicilio = "MARIA A HALLACK CALLE 12 y 9 0 1629 Pilar Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060638")

    def testMunicipio_domicilioContiene_SANISIDROBuenosAiresAR(self):
        # Arrange
        domicilio = "FONDO DE LA LEGUA 936 B1640EDO MARTINEZ / SAN ISIDRO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060756")

    def testMunicipio_domicilioContiene_TigreBuenosAiresAR(self):
        # Arrange
        domicilio = "Austria Norte (esquina Pehuajo, Parque Industrial Tigre ) 1037 1617 Tigre Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_ElTalarBuenosAiresAR(self):
        # Arrange
        domicilio = "Marcos Sastre (-34.4905358,-58.6847448,12.25) 1034 1618 El Talar Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_TRESDEFEBREROBuenosAiresAR(self):
        # Arrange
        domicilio = "RIO NEGRO 10257 B1690AQK 11 DE SEPTIEMBRE / TRES DE FEBRERO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060840")

    def testMunicipio_domicilioContiene_VICENTELOPEZBuenosAiresAR(self):
        # Arrange
        domicilio = "UGARTE GOBERNADOR MARCELINO PISO 1 3610 B1605EJO MUNRO / VICENTE LOPEZ Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060861")

    def testMunicipio_domicilioContiene_ChivilcoyBuenosAiresAR(self):

        # Arrange
        domicilio = "calle 112 N? 82 ( 34?54 57.71 S  /  59?59 31.10 O) 0 6620 Chivilcoy Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060224")

    def testMunicipio_domicilioContiene_TRENQUELAUQUENBuenosAiresAR(self):

        # Arrange
        domicilio = "AV GREGORIO R CUELLO 475 B6400AUE TRENQUE LAUQUEN / TRENQUE LAUQUEN Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060826")

    def testMunicipio_domicilioContiene_BellaVistaBuenosAiresAR(self):

        # Arrange
        domicilio = "Av. Pte. Illia 241,  -34.563505,-58.659458 - 1661 Bella Vista Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060760")

    def testMunicipio_domicilioContiene_BeccarBuenosAiresAR(self):

        # Arrange
        domicilio = "Quito  -Piso 1 2618 1643 Beccar Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060756")

    def testMunicipio_domicilioContiene_GENERALPACHECOBuenosAiresAR(self):

        # Arrange
        domicilio = "AUSTRIA 1200 1617 GENERAL PACHECO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_PACHECOBuenosAiresAR(self):

        # Arrange
        domicilio = "COLOMBIA 1341 1111 EL TALAR DE PACHECO / PACHECO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_TANDILBuenosAiresAR(self):

        # Arrange
        domicilio = "PTE H YRIGOYEN 560 B7000ANB TANDIL / TANDIL Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060791")

    def testMunicipio_domicilioContiene_TortuguitasBuenosAiresAR(self):

        # Arrange
        domicilio = "OTTO KRAUSE 4903 1677 Tortugitas Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060515")

    def testMunicipio_domicilioContiene_MelchorRomeroBuenosAiresAR(self):

        # Arrange
        domicilio = "Avda. 520 y Ruta Prov. 36 9497 1903 Melchor Romero Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060441")

    def testMunicipio_domicilioContiene_tigreBuenosAiresAR(self):

        # Arrange
        domicilio = "Av. de los Lagos Centro Comercial Nordelta 7010 1670 tigre Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_ElJaguelBuenosAiresAR(self):

        # Arrange
        domicilio = "Estancia los Remedios (34?49&apos;3.55&quot;S - 58?30&apos;22.12&quot;O) 2701 1805 El Jaguel " \
                    "Buenos Aires AR "

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060260")

    def testMunicipio_domicilioContiene_GENERALPUEYRREDONBuenosAiresAR(self):

        # Arrange
        domicilio = "BELGRANO DOCTOR MANUEL 2885 B7600GKQ MAR DEL PLATA / GENERAL PUEYRREDON Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060357")

    def testMunicipio_domicilioContiene_ElPatoBuenosAiresAR(self):

        # Arrange
        domicilio = "RUTA 2 KM 39 lote 21 ( PARQ INDUS. PIBERA) 0 1893 El Pato Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060091")

    def testMunicipio_domicilioContiene_benavidezBuenosAiresAR(self):

        # Arrange
        domicilio = "Av. Gral. Juan Domingo Peron 7245 1621 benavidez Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_GarinBuenosAiresAR(self):

        # Arrange
        domicilio = "Colectora Panamericana Km 40  (34?24&apos;50.1&quot;S 58?43&apos;34.1&quot;W) 0 1619 Garin Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060252")

    def testMunicipio_domicilioContiene_TIGREBuenosAiresAR(self):

        # Arrange
        domicilio = "PANAMERICANA 23951 B1611KSE DON TORCUATO / TIGRE Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_PachecoBuenosAiresAR(self):

        # Arrange
        domicilio = "Alferez Martinez de Alegria 2386 (Ex-Gelly Obes) - 1617 Pacheco Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060805")

    def testMunicipio_domicilioContiene_MartinezBuenosAiresAR(self):

        # Arrange
        domicilio = "Colectora Panamericana (Esq Yapeyu) 264 1640 Martinez Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060756")

    def testMunicipio_domicilioContiene_IngPabloNoguesBuenosAiresAR(self):

        # Arrange
        domicilio = "J. Stephenson (entre ruta 197 y Bailen) 50 1616 Ing. Pablo Nogues Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060515")

    def testMunicipio_domicilioContiene_MarChiquitaBuenosAiresAR(self):

        # Arrange
        domicilio = "VENECIA 1101 B7609MLC SANTA CLARA DEL MAR / MAR CHIQUITA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060518")

    def testMunicipio_domicilioContiene_LAPLATABuenosAiresAR(self):
        # Arrange
        domicilio = "CALLE 50 488 B1900AST LA PLATA / LA PLATA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060441")

    def testMunicipio_domicilioContiene_MALVINASARGENTINASBuenosAiresAR(self):
        # Arrange
        domicilio = "AV DEL SESQUICENTENARIO 1701 1111 PABLO NOGUES / MALVINAS ARGENTINAS Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060515")

    def testMunicipio_domicilioContiene_SANFERNANDOBuenosAiresAR(self):
        # Arrange
        domicilio = "BELGRANO MANUEL 1182 B1646CQX SAN FERNANDO / SAN FERNANDO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060749")

    def testMunicipio_domicilioContiene_ITUZAINGOBuenosAiresAR(self):
        # Arrange
        domicilio = "JUNCAL 34 B1714MED ITUZAINGO / ITUZAINGO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060410")

    def testMunicipio_domicilioContiene_OlivosBuenosAiresAR(self):
        # Arrange
        domicilio = "RUTA NAC. PANAMERICANA(Ascendente) Y ACC DEBENEDETTI apies 2985 1640 Olivos Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060861")

    def testMunicipio_domicilioContiene_MalvinasArgentinasBuenosAiresAR(self):
        # Arrange
        domicilio = "RUTA PANAMERICANA - COLECTORA OESTE 1 - s/n 1667 Malvinas Argentinas Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060515")

    def testMunicipio_domicilioContiene_FLORENCIOVARELABuenosAiresAR(self):
        # Arrange
        domicilio = "MONTEAGUDO B DE 203 3031 1888 FLORENCIO VARELA / FLORENCIO VARELA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060274")

    def testMunicipio_domicilioContiene_CAMPANABuenosAiresAR(self):
        # Arrange
        domicilio = "KM40,5 RN9 (GPS 34?24&apos;18.7&quot;S 58?43&apos;52.7&quot;W) 0 2804 CAMPANA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060126")

    def testMunicipio_domicilioContiene_LaRejaBuenosAiresAR(self):
        # Arrange
        domicilio = "Colectora Sur Acceso Oeste KM 41 s/n 1738 La Reja Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060560")

    def testMunicipio_domicilioContiene_PilarBuenosAiresAR(self):
        # Arrange
        domicilio = "INDEPENDENCIA 6695 B1669FYM DEL VISO / PILAR Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060638")

    def testMunicipio_domicilioContiene_ZarateBuenosAiresAR(self):
        # Arrange
        domicilio = "Camino de la Costa Brava y Calle 42 0 2800 zarate Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060882")

    def testMunicipio_domicilioContiene_LAMATANZABuenosAiresAR(self):
        # Arrange
        domicilio = "AV BDIER GRAL J M DE ROSAS 3801 B1754FUJ SAN JUSTO / LA MATANZA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060427")

    def testMunicipio_domicilioContiene_MARDELPLATABuenosAiresAR(self):
        # Arrange
        domicilio = "ESQUEL (ESTACION CAMET) 1251 7600 MAR DEL PLATA / MAR DEL PLATA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060357")

    def testMunicipio_domicilioContiene_QUILMESBuenosAiresAR(self):
        # Arrange
        domicilio = "CALCHAQUI 5500 B1840BWN EZPELETA OESTE / QUILMES Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060658")

    def testMunicipio_domicilioContiene_CAMPANABuenosAiresAR(self):
            # Arrange
        domicilio = "KM40,5 RN9 (GPS 34?24&apos;18.7&quot;S 58?43&apos;52.7&quot;W) 0 2804 CAMPANA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060126")

    def testMunicipio_domicilioContiene_VILLABOSCHBuenosAiresAR(self):
            # Arrange
        domicilio = "PRESIDENTE PERON 1001 1682 VILLA BOSCH Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060840")

    def testMunicipio_domicilioContiene_SanAndresBuenosAiresAR(self):
            # Arrange
        domicilio = "Diagonal 80 Diagonal Servet 1325 1651 San Andres Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060371")

    def testMunicipio_domicilioContiene_OLAVARRIABuenosAiresAR(self):
            # Arrange
        domicilio = "DE LOS INMIGRANTES 1426 B7436AMR LOMA NEGRA / OLAVARRIA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060595")

    def testMunicipio_domicilioContiene_LUJANBuenosAiresAR(self):
            # Arrange
        domicilio = "RP34 INTERSECCION RP6 34 33 23.3 S 59 02 09 0 W. 0 B6702DCA LUJAN / LUJAN Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060497")

    def testMunicipio_domicilioContiene_VillarinoViejoBuenosAiresAR(self):
            # Arrange
        domicilio = "Av Placida Pernici S/N (38?41&apos;44.43&quot;S-62?27&apos;53.77&quot;O) 0 8132 Villarino Viejo Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060875")

    def testMunicipio_domicilioContiene_AVELLANEDABuenosAiresAR(self):
            # Arrange
        domicilio = "ORTEGA 5500 B1875CCA WILDE / AVELLANEDA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060035")

    def testMunicipio_domicilioContiene_SANPEDROBuenosAiresAR(self):

        # Arrange
        domicilio = "RUTA 9 KM 174 - 33? 42`31,06 S. 59? 50`12,79 O 0 B2930LJA SAN PEDRO / SAN PEDRO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060770")

    def testMunicipio_domicilioContiene_LANUSBuenosAiresAR(self):

        # Arrange
        domicilio = "AV HIPOLITO YRIGOYEN 5201 B1824ABI LANUS / LANUS Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060434")

    def testMunicipio_domicilioContiene_PIlarBuenosAiresAR(self):

        # Arrange
        domicilio = "Carlos Calvo 0, La Lonja -34.437451, -58.831583 0 1635 PIlar Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060638")

    def testMunicipio_domicilioContiene_MERLOBuenosAiresAR(self):

        # Arrange
        domicilio = "MORENO DOCTOR MARIANO 560 B1722CZF MERLO / MERLO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060539")

    def testMunicipio_domicilioContiene_MoronBuenosAiresAR(self):

        # Arrange
        domicilio = "Stevenson y Callao (-34.664954 -58.729205) s/n 1708 Moron Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060568")

    def testMunicipio_domicilioContiene_ZarateBuenosAiresAR(self):

        # Arrange
        domicilio = "Parque Logistico Industrial Coordenadas -34.084389 59.066750 0 - Zarate Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual(municipio_obtenido, "060882")

    def testMunicipio_domicilioContiene_9DEABRILBuenosAiresAR(self):

        # Arrange
        domicilio = "MARIANA ARBEL (GPS -34.775037, -58.472254) 3451 1839 ESTEBAN ECHEVERRIA - 9 DE ABRIL Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060260", municipio_obtenido)

    def testMunicipio_domicilioContiene_TortuguitasBuenosAiresAR(self):

        # Arrange
        domicilio = "Hooke 3905 (34?27&apos;46.4&quot;S 58?42&apos;42.8&quot;W) 0 1615 Tortuguitas Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060515", municipio_obtenido)

    def testMunicipio_domicilioContiene_PachecoBuenosAiresAR(self):

        # Arrange
        domicilio = "Ruta Panamericana KM 32.300, 0 1617 Pacheco, Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060805", municipio_obtenido)

    def testMunicipio_domicilioContiene_NORDELTABuenosAiresAR(self):

        # Arrange
        domicilio = "BOULEVARD DE LA PLAZA 255 1670 NORDELTA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060805", municipio_obtenido)

    def testMunicipio_domicilioContiene_CORONELROSALESBuenosAiresAR(self):

        # Arrange
        domicilio = "CORONEL ROSALES Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060182", municipio_obtenido)

    def testMunicipio_domicilioContiene_BAHIABLANCABuenosAiresAR(self):

        # Arrange
        domicilio = "SAAVEDRA 144 B8000DDD BAHIA BLANCA / BAHIA BLANCA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060056", municipio_obtenido)

    def testMunicipio_domicilioContiene_DONTORCUATOBuenosAiresAR(self):

        # Arrange
        domicilio = "LUGONES LEOPOLDO 3299 B1610CGC DON TORCUATO / DON TORCUATO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060805", municipio_obtenido)

    def testMunicipio_domicilioContiene_LAREJABuenosAiresAR(self):

        # Arrange
        domicilio = "Av. Gaona (Colectora Sur Acceso Oeste Km 41) - Local 6 11024 1738 LA REJA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060560", municipio_obtenido)

    def testMunicipio_domicilioContiene_RICARDOROJASBuenosAiresAR(self):

        # Arrange

        domicilio = "086_B - Av. Henry Ford 3200 1610 RICARDO ROJAS Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060805", municipio_obtenido)

    def testMunicipio_domicilioContiene_MaschwitzBuenosAiresAR(self):

        # Arrange
        domicilio = "Colectora Este Panamericana Ramal Escobar Km 42.7 - 1623 Maschwitz Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060252", municipio_obtenido)

    def testMunicipio_domicilioContiene_JOSECLEMENTPAZBuenosAiresAR(self):

        # Arrange
        domicilio = "CAMPOS GASPAR 5690 B1665GVU JOSE CLEMENTE PAZ / JOSE CLEMENTE PAZ Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060412", municipio_obtenido)

    def testMunicipio_domicilioContiene_PehuajoBuenosAiresAR(self):

        # Arrange
        domicilio = "RUTA NACIONAL 5 KM 383 (Ascendente) - APIES 1611 0 6472 Pehuajo Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060609", municipio_obtenido)

    def testMunicipio_domicilioContiene_ZARATEBuenosAiresAR(self):

        # Arrange
        domicilio = "ITUZAINGO 654 B2800JFL ZARATE / ZARATE Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060882", municipio_obtenido)

    def testMunicipio_domicilioContiene_MardelPlataBuenosAiresAR(self):

        # Arrange
        domicilio = "RUTA PROVINCIAL 88 KM (Ascendente) Y VERTIZ - APIES 31211 0 7630 Mar del Plata Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060357", municipio_obtenido)

    def testMunicipio_domicilioContiene_sanmiguelBuenosAiresAR(self):

        # Arrange
        domicilio = "Ruta Nac 8 y Ruta 202, APIES 321 -34.57289 -58.69231 0 1661 san miguel Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060760", municipio_obtenido)

    def testMunicipio_domicilioContiene_gregoriodelaferrereBuenosAiresAR(self):

        # Arrange
        domicilio = "Ruta 21 y Carlos Casares 0 1757 gregorio de laferrere Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060427", municipio_obtenido)

    def testMunicipio_domicilioContieneVillaBallesterBuenosAiresAR(self):

        # Arrange
        domicilio = "122 Ex Roca 4785 1653 Villa Ball / Villa Ballester Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060371", municipio_obtenido)

    def testMunicipio_domicilioContieneEZEIZABuenosAiresAR(self):

        # Arrange
        domicilio = "NORBERTO LOPEZ 100 1802 EL JAGUEL / EZEIZA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060270", municipio_obtenido)

    def testMunicipio_domicilioContienePERGAMINOBuenosAiresAR(self):
        # Arrange
        domicilio = "Bv. Dardo Rocha 280 J5402DCV PERGAMINO Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060623", municipio_obtenido)

    def testMunicipio_domicilioContienFatimaOBuenosAiresAR(self):
        # Arrange
        domicilio = "Belgrano 596 1633 Fatima Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060638", municipio_obtenido)

    def testMunicipio_domicilioContienFATIMAOBuenosAiresAR(self):
        # Arrange
        domicilio = "CALLE 9 (Edificio: Celsur Logistica S.A., Room: Server) 1430 1633 FATIMA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060638", municipio_obtenido)

    def testMunicipio_domicilioContienemardelplataBuenosAiresAR(self):
        # Arrange
        domicilio = "Suc 684 - Av. Independencia 1941 7600 mar del plata Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060357", municipio_obtenido)

    def testMunicipio_domicilioContieneVillaBoschBuenosAiresAR(self):
        # Arrange
        domicilio = "Juan Domingo Peron 1001 1682 Villa Bosch Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060840", municipio_obtenido)

    def testMunicipio_domicilioContieneCOLONBuenosAiresAR(self):
        # Arrange
        domicilio = "BOULEVAR 50 (GPS 33?54&apos;10.07&quot;S 61? 6&apos;24.12&quot;O) 431 2720 COLON Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060175", municipio_obtenido)

    def testMunicipio_domicilioContieneLACOSTABuenosAiresAR(self):
        # Arrange
        domicilio = "Calle39 (Altura 234/258) 0 B7107CDF SANTA TERESITA / LA COSTA Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060420", municipio_obtenido)

    def testMunicipio_domicilioContieneSANTOSLUGARESBuenosAiresAR(self):
        # Arrange
        domicilio = "SENADOR FERRO Y R ARGENTINO (-34.607450, -58.541506) 0 1676 SANTOS LUGARES Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060840", municipio_obtenido)

    def testMunicipio_domicilioContieneBenavidezBuenosAiresAR(self):
        # Arrange
        domicilio = "Av Pte Peron 4749 -ex Constituyentes) 0 1621 Benavidez Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060805", municipio_obtenido)

    def testMunicipio_domicilioContieneColonBuenosAiresAR(self):
        # Arrange
        domicilio = "(Acopio) - Calle 44 y 127 0 2720 Colon Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060175", municipio_obtenido)

    def testMunicipio_domicilioContieneEstebanEcheverriaBuenosAiresAR(self):
        # Arrange
        domicilio = "Av Camino de Cintura 7732 1839 Esteban Echeverria Buenos Aires AR"

        # Act
        municipio_obtenido = codigo_municipio(domicilio)

        # Assert
        self.assertEqual("060260", municipio_obtenido)


if __name__ == '__main__':
    unittest.main()
