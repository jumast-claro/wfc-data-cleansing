import unittest
import os
import sys
CURRENT = os.path.dirname(os.path.abspath(__file__))
PARENT = os.path.dirname(CURRENT)
sys.path.append(PARENT)
from src.utils import etapa


class TestEtapa(unittest.TestCase):

    def test_DefinicionDeProyecto(self):

        # Arrange
        descripcion = "9000031686 - ALTA - [MTpd] Definicion de proyecto"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Definición de proyecto")

    def test_PermisosMunicipales(self):
        # Arrange
        descripcion = "6000000659 - MEJORA_RED - [MTsp] Permisos municipales"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Permisos municipales")

    def test_ObraCivil(self):
        # Arrange
        descripcion = "9000015961 - ALTA - [MTct] Realizar obra civil"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Obra civil")

    def test_Tendidos(self):

        # Arrange
        descripcion = "9000015933 - ALTA - [MTcl] Tendidos"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Tendidos")

    def test_Cancelacion(self):

        # Arrange
        descripcion = "9000031679 - ALTA - [MTcf] Cancelacion"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Cancelación")

    def test_IngenieriaDeClientesAnexarCopia(self):

        # Arrange
        descripcion = "6000000156 - MODIFICACION_OTI - [MTci] Ingenieria de clientes (anexar-copia)"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Anexada")

    def test_DisenioTecnicoAnexarCopia(self):
        # Arrange
        descripcion = "9000006536 - MODIFICACION_SERVICIO - [MTdt] Dise?o tecnico* (anexar-copia)"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Anexada")

    def test_TrabajoDeInfraescructuraAnexarCopia(self):
        # Arrange
        descripcion = "000006944 - MODIFICACION_TECNICA - [MTtp] Trabajo de Infraestructura - Acceso (anexar-copia)"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Anexada")

    def test_TrabajoDeInfraescructuraInDoorAnexarCopia(self):
        # Arrange
        descripcion = "9000015926 - MODIFICACION_TECNICA - [MTtp] Trabajo de Infraestructura - InDoor (anexar-copia)"

        # Act
        etapa_obtenida = etapa(descripcion)

        # Assert
        self.assertEqual(etapa_obtenida, "Anexada")


