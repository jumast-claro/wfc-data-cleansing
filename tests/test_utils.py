import unittest
from src.utils import l2l


class TestUtils(unittest.TestCase):

    def test_l2l_descripcionContiene_SITIOA(self):

        # Arrange
        descripcion = "Orden: 9000005457 - ALTA - [MTsp] Permisos municipales - SITIO A"

        # Act
        obtenido = l2l(descripcion)

        # Assert
        self.assertEqual("A", obtenido)

    def test_l2l_descripcionContiene_SITIOB(self):

        # Arrange
        descripcion = "Orden: 9000005457 - ALTA - [MTpd] Definicion de proyecto - SITIO B"

        # Act
        obtenido = l2l(descripcion)

        # Assert
        self.assertEqual("B", obtenido)

    def test_l2l_descripcionNoContieneNiSITIOANiSITIOB(self):

        # Arrange
        descripcion = "Orden: 9000005457 - ALTA - [MTpd] Definicion de proyecto"

        # Act
        obtenido = l2l(descripcion)

        # Assert
        self.assertEqual("", obtenido)

    def test_l2l_descripcionContieneLetraA(self):

        # Arrange
        descripcion = "Orden: 9000005457 - ALTA - [MTpd] Definicion de proyecto A"

        # Act
        obtenido = l2l(descripcion)

        # Assert
        self.assertEqual("", obtenido)

    def test_l2l_descripcionContieneLetraB(self):

        # Arrange
        descripcion = "Orden: 9000005457 - ALTA - [MTpd] Definicion de proyecto B"

        # Act
        obtenido = l2l(descripcion)

        # Assert
        self.assertEqual("", obtenido)


