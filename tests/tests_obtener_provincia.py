import unittest
from src.utils import codigo_provincia


class TestObtenerProvincia(unittest.TestCase):

    def testCodigoProvincia_domicilioContiene_SaltaAR(self):

        # Arrange
        domicilio = "Av. M J Tavella (24 49'22.01? S ? 65 25'07.70? O) 2550 4400 salta Salta AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-A")

    def testCodigoProvincia_domicilioContiene_BuenosAiresAR(self):

        # Arrange
        domicilio = "AV. ANTARTIDA ARGENTINA Y CALLE 258 0 1886 RANELAGH / BERAZATEGUI Buenos Aires AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-B")

    def testCodigoProvincia_domicilioContiene_CapitalFederalAR(self):

        # Arrange
        domicilio = "AV JOSE M MORENO 557 C1424AAF CIUDAD AUTONOMA BUENOS AIRES / CAPITAL FEDERAL Capital Federal AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-C")

    def testCodigoProvincia_domicilioContiene_SanLuisAR(self):

        # Arrange
        domicilio = "Calle 106 e/6 y 8 Parque Industrial Sur - 5700 San Luis San Luis AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-D")

    def testCodigoProvincia_domicilioContiene_LaRiojaAR(self):

        # Arrange
        domicilio = "Puerto Argentino esq. El Chacho 0 5300 La Rioja La Rioja AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-F")

    def testCodigoProvincia_domicilioContiene_SantiadoDelEsteroAR(self):

        # Arrange
        domicilio = "LIBERTAD 688 G4200CZT SANTIAGO DEL ESTERO / CAPITAL Santiago del Estero AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-G")

    def testCodigoProvincia_domicilioContiene_CatamarcaAR(self):

        # Arrange
        domicilio = "CHACABUCO 169 K4700BTC SAN FDO DEL VALLE DE CATAMARCA / CAPITAL Catamarca AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-K")

    def testCodigoProvincia_domicilioContiene_MendozaAR(self):

        # Arrange
        domicilio = "Salta 672, / Ref: Ministerio de Seguridad (BackUp) - 5501 Godoy Cruz Mendoza AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-M")

    def testCodigoProvincia_domicilioContiene_NeuquenAR(self):

        # Arrange
        domicilio = "BACHMAN ERNESTO 149 Q8316DQA PLOTTIER / CONFLUENCIA Neuquen AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-Q")

    def testCodigoProvincia_domicilioContiene_SantaFeAR(self):

        # Arrange
        domicilio = "Alvear 1598, - S2000QGP ROSARIO / ROSARIO Santa Fe AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-S")

    def testCodigoProvincia_domicilioContiene_TucumanAR(self):

        # Arrange
        domicilio = "MAIPU BATALLA DE (oficina F) 50 T4000IMB SAN MIGUEL DE TUCUMAN / CAPITAL Tucuman AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-T")

    def testCodigoProvincia_domicilioContiene_ChubutAR(self):

        # Arrange
        domicilio = "RN3 KM 1845,423 (-45,931270, -67,596480) 0. 9001 Rada Tilly Chubut AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-U")

    def testCodigoProvincia_domicilioContiene_CordobaAR(self):

        # Arrange
        domicilio = "Av Monse?or Pablo Cabrera 4809 5008 Nuevo Poeta Lugones Cordoba AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-X")

    def testCodigoProvincia_domicilioContiene_SantaCruzAR(self):

        # Arrange
        domicilio = "OBRERO SAN JOSE 1357 Z9011HRA CALETA OLIVIA / DESEADO Santa Cruz AR"

        # Act
        provincia_obtenida = codigo_provincia(domicilio)

        # Assert
        self.assertEqual(provincia_obtenida, "AR-Z")

if __name__ == '__main__':
    unittest.main()

