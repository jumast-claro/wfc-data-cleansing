import unittest
import os
import sys
CURRENT = os.path.dirname(os.path.abspath(__file__))
PARENT = os.path.dirname(CURRENT)
sys.path.append(PARENT)
from src.utils import tipo


class TestTipo(unittest.TestCase):

    def test_ALTA(self):

        # Arrange
        descripcion = "9000005298 - ALTA - [MTpd] Definicion de proyecto"

        # Act
        tipo_calculado = tipo(descripcion)

        # Assert
        self.assertEqual("ALTA", tipo_calculado)

    def test_MEJORA_RED(self):

        # Arrange
        descripcion = "6000000262 - MEJORA_RED - [MTif] Inst FO"

        # Act
        tipo_calculado = tipo(descripcion)

        # Assert
        self.assertEqual("MEJORA_RED", tipo_calculado)

    def test_MODIFICACION_OTI(self):
        # Arrange
        descripcion = "6000000156 - MODIFICACION_OTI - [MTci] Ingenieria de clientes (anexar-copia)"

        # Act
        tipo_calculado = tipo(descripcion)

        # Assert
        self.assertEqual("MODIFICACION_OTI", tipo_calculado)

    def test_MODIFICACION_TECNICA(self):
        # Arrange
        descripcion = "9000006944 - MODIFICACION_TECNICA - [MTtp] Trabajo de Infraestructura - Acceso (anexar-copia)"

        # Act
        tipo_calculado = tipo(descripcion)

        # Assert
        self.assertEqual("MODIFICACION_TECNICA", tipo_calculado)

    def test_CualquierOtraDescripcionQuePuedaLlegarAAparecer(self):
        # Arrange
        descripcion = "9000006944 - XXXYYYZZZ - [MTtp] Trabajo de Infraestructura - Acceso (anexar-copia)"

        # Act
        tipo_calculado = tipo(descripcion)

        # Assert
        self.assertEqual("XXXYYYZZZ", tipo_calculado)

    def test_FormatoDesconocido(self):

        # Arrange
        descripcion = "9000006944 - [MTtp] Trabajo de Infraestructura"

        # Act
        tipo_calculado = tipo(descripcion)

        # Assert
        self.assertEqual("?", tipo_calculado)
